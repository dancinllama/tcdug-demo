/**
 * CloseAllOpptExt
 * @description Extension for updating child records
 * Demo for TCDUG - Deployments
 * @author James Loghry
 */
public with sharing class CloseAllOpptyExt{

    List<Opportunity> oppties {get; private set;}

    public CloseAllOpptyExt(ApexPages.StandardSetController ssc){
        oppties = (List<Opportunity>)ssc.getSelected();
    }
    
    public PageReference closeRelatedOppties(){
        for(Opportunity oppty : oppties){
            //Using Closed for Demo purposes..  should be a customs setting, or something more configurable, ideally
            oppty.StageName = 'Closed Won';
        }
        return null;
    }
}